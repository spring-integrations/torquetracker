# Torque Tracker

## Introduction
This sample shows some ideas and principles I found out about Spring Messaging/Spring Cloud Stream pipelines
It especially shows how to use:
- Http Inbound Gateway (how to send messages originating from a HTTP request into a pipeline)
- RabbitMQ Outbound through Spring Cloud Stream (how to send messages to Rabbit MQ with Spring Cloud Stream)
- MQTT Outbound (how to send messages to an MQTT broker)
- Measurement/Unit API (how unit conversion can be done)

## The application
This application receives vehicle data from [Torque](https://play.google.com/store/apps/details?id=org.prowl.torque&hl=en_US) through an HTTP GET request.
Those requests look like: 

`http://192.168.1.245/?eml=bla@bla.nl&v=8&session=1551799816170&id=9bd44e6f75c5cd17914e49d226aa27e8&time=1551799836563&kff1005=5.80052193&kff1006=51.80822237&kff1001=0.0&kff1007=0.0&kff1223=0.029517&kff1220=0.029531432&kff1221=-0.024162082&kff1222=0.9284579&kff129a=8.0&kff1270=1001.0076&kff1239=4.288&kff1010=59.3167724609375&kff123b=0.0&kff1006=51.80822237&kff1005=5.80052193&kff123a=11.0&kff1204=0.0, id=4520fa36-5cbf-30b7-c603-b0726176c1b0, timestamp=1551799858700}`

The query string contains tags with their values, and every tag stands for a specific quantity (e.g. `kff1005` is longitude).
However this is Torque specific, and also the values are in Torque specific units. Therefore some mapping and unit conversion is 
done to 'internal units' which are both configurable.

An example of a message that is output to MQTT/RabbitMQ:

`{"timestamp":1551798150,"lat":51.8082472,"lon":5.80057736,"tagValues":{"DEVICE_BATTERY_LEVEL":11.0,"DEVICE_AIR_PRESSURE":1000.9693,"GPS_SPEED":0.0,"ACC_TOTAL":0.030015465,"ACC_Y":-0.022452662,"ACC_X":0.031239297,"ACC_Z":0.92894477}}`


## Messaging pipeline

- HTTP GET -->[httpInboundGateway]----->(--torqueInboundChannel--)----->[@ServiceActivator TorqueDataSource]
- [@ServiceActivator TorqueDataSource]------>[httpInboundGateway] ----> "HTTP 200 OK!" (through a reply channel)
- [@ServiceActivator TorqueDataSource]------> (--dataSourceInboundChannel--) ---> [@Transformer VehicleDataProcessor.processValues] 
- [@Transformer VehicleDataProcessor.processValues] ---> (--vehicleDataChannel--) ----> RabbitMQ
- [@Transformer VehicleDataProcessor.processValues] ---> (--vehicleDataChannel--) ----> [@Transformer VehicleDataMqttTransformer.transform]  
 -[@Transformer VehicleDataMqttTransformer.transform] ---> (--mqttOutChannel--) ---> [MessageHandler mqttOutbound] ---> MQTT 

## Message payloads 
- torqueInboundChannel
  - Map<String, String> with all query parameters and values  
  
- dataSourceInboundChannel
  - Map<String, Double> with all numeric parameters
  
- vehicleDataChannel
  - VehicleData object containing timestamp, latitude and longitude and all other parameters mapped to internal names and converted to internal units
 
- mqttOutChannel
  - Since the mqtt MessageHandler only support String messages, this channel will have a JSON string representation of the VehicleData object


## How to run:

- Have the Torque application installed on a mobile device
- Enable web logging, set url to http://<hostname>:8080/
- Configure the tags that will be logged (see tag_mapping.json for which ones, latitude & longitude will be automatically logged)
- Make sure you have RabbitMQ and/or Mosquitto installed (initially only MQTT support is enabled)
- Run `TorquetrackerApplication` from your ide or use `mvn spring-boot:run` from command line

### Enable RabbitMQ
- To enable rabbitmq support uncomment the following lines:
  - The commented lines in `pom.xml`
  - Uncomment `@EnableBinding(VehicleDataBinding.class)` in BeanConfiguration.java
  - Comment ``` @Bean(name="vehicleDataChannel")
                   public MessageChannel vehicleDataChannel() {
                       return new DirectChannel();
                   }```
 
                                                                                                           