package com.joco.torquetracker.vehicledata.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Getter
@ToString
public class VehicleData {

    @Setter
    private Long timestamp;

    @Setter
    private Double lat;

    @Setter
    private Double lon;

    private Map<String, Double> tagValues;

    public VehicleData() {
        tagValues = new HashMap<>();
    }
}
