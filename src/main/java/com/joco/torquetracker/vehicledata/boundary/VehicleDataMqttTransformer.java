package com.joco.torquetracker.vehicledata.boundary;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.joco.torquetracker.vehicledata.entity.VehicleData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class VehicleDataMqttTransformer {
    private ObjectMapper mapper;

    public VehicleDataMqttTransformer() {
        this.mapper = new ObjectMapper();
    }

     @Transformer(inputChannel = "vehicleDataChannel", outputChannel = "mqttOutChannel")
    public String transform(VehicleData input) throws JsonProcessingException {
        log.info("Sending to mqtt");
        return mapper.writeValueAsString(input);
    }
}
