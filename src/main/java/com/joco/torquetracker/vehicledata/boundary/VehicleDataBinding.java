package com.joco.torquetracker.vehicledata.boundary;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface VehicleDataBinding {
    @Output("vehicleDataChannel")
    MessageChannel vehicleDataOutputChannel();
}
