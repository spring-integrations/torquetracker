package com.joco.torquetracker.vehicledata.control;

import com.joco.torquetracker.configuration.control.ConfigurationService;
import com.joco.torquetracker.configuration.entity.MappingDetails;
import com.joco.torquetracker.configuration.boundary.SourceTagMapper;
import com.joco.torquetracker.vehicledata.entity.VehicleData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Component;
import tec.units.ri.format.SimpleUnitFormat;

import javax.measure.IncommensurableException;
import javax.measure.Unit;
import javax.measure.UnitConverter;
import javax.measure.format.ParserException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;

import static com.joco.torquetracker.Utils.*;

@Component
@Slf4j
public class VehicleDataProcessor {


    private final ConfigurationService configurationService;
    private final SourceTagMapper sourceTagMapper;

    @Autowired
    public VehicleDataProcessor(ConfigurationService configurationService, SourceTagMapper sourceTagMapper) {
        this.configurationService = configurationService;
        this.sourceTagMapper = sourceTagMapper;
    }

    @Transformer(inputChannel = "dataSourceInboundChannel", outputChannel = "vehicleDataChannel")
    public VehicleData processValues(Map<String, Double> sourceValues)  {

        VehicleData vehicleData = new VehicleData();


        for (String sourceTag : sourceValues.keySet()) {

            MappingDetails tagMapping = sourceTagMapper.getMapping().get(sourceTag);
            if (tagMapping == null) {
                log.warn("Tag {} has no mapping to an internal tag, so the value is ignored.", sourceTag);
                continue;
            }
            switch (tagMapping.getInternalTag()) {
                case LAT_TAG:
                    vehicleData.setLat(sourceValues.get(sourceTag));
                    break;
                case LON_TAG:
                    vehicleData.setLon(sourceValues.get(sourceTag));
                    break;
                default: {
                    String sourceUnit = tagMapping.getSourceUnit();
                    String targetUnit = configurationService.getConfiguration().stream().filter(f -> f.getName().equals(tagMapping.getInternalTag())).findAny().orElse(null).getUnit();
                    try {
                        Double internalValue = calculateInternalValue(sourceValues.get(sourceTag), sourceUnit, targetUnit);
                        if (tagMapping.getInternalTag().equals(EVENT_TIME_TAG)) {
                            vehicleData.setTimestamp(internalValue.longValue());
                        }
                        else {
                            vehicleData.getTagValues().put(tagMapping.getInternalTag(), internalValue);
                        }
                    } catch (IncommensurableException | ParserException ex) {
                        log.error("Cannot convert {} to {} (source tag={}, internal tag={}), so the value is ignored", sourceUnit, targetUnit, sourceTag, tagMapping.getInternalTag(), ex);
                    }
                }
            }
        }

        if (vehicleData.getTimestamp() == null) {
            try {
                log.info("No event timestamp was provided in the data, so setting current time");
                Double currentUtcTime = (double) OffsetDateTime.now(ZoneOffset.UTC).toEpochSecond();
                String sourceUnit = "s";
                String targetUnit = configurationService.getConfiguration().stream().filter(f -> f.getName().equals(EVENT_TIME_TAG)).findAny().orElse(null).getUnit();
                vehicleData.setTimestamp(calculateInternalValue(currentUtcTime, sourceUnit, targetUnit).longValue());
            } catch (IncommensurableException e) {
                throw new InvalidTimestampException(String.format("Invalid timestamp!"));
            }
        }

        if (vehicleData.getLat() == null || vehicleData.getLon() == null) {
            throw new InvalidLocationException(String.format("No location was provided for timestamp t=%d", vehicleData.getTimestamp()));
        }


        return vehicleData;

    }

    private Double calculateInternalValue(Double sourceValue, String sourceUnitString, String destinationUnitString) throws IncommensurableException {
        Unit<?> sourceUnit = SimpleUnitFormat.getInstance().parse(sourceUnitString);
        Unit<?> destUnit = SimpleUnitFormat.getInstance().parse(destinationUnitString);
        UnitConverter converter = sourceUnit.getConverterToAny(destUnit);
        return converter.convert(sourceValue).doubleValue();
    }
}
