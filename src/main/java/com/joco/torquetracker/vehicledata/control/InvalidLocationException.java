package com.joco.torquetracker.vehicledata.control;

public class InvalidLocationException extends RuntimeException {
    public InvalidLocationException() {
    }

    public InvalidLocationException(String s) {
        super(s);
    }

    public InvalidLocationException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InvalidLocationException(Throwable throwable) {
        super(throwable);
    }

    public InvalidLocationException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
