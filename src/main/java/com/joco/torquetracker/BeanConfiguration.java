package com.joco.torquetracker;

import com.joco.torquetracker.vehicledata.boundary.VehicleDataBinding;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.http.inbound.HttpRequestHandlingMessagingGateway;
import org.springframework.integration.http.inbound.RequestMapping;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.converter.MessageConverter;

@Configuration
//@EnableBinding(VehicleDataBinding.class)
public class BeanConfiguration {

    @Bean
    public HttpRequestHandlingMessagingGateway httpInboundGateway() {
        HttpRequestHandlingMessagingGateway gateway = new HttpRequestHandlingMessagingGateway(true);
        RequestMapping requestMapping = new RequestMapping();
        requestMapping.setMethods(HttpMethod.GET);
        requestMapping.setPathPatterns("/");

        gateway.setRequestMapping(requestMapping);
        gateway.setRequestChannelName("torqueInboundChannel");

        return gateway;
    }

    @Bean(name="torqueInboundChannel")
    public MessageChannel torqueInboundChannel() {
        return new DirectChannel();
    }

    @Bean(name="dataSourceInboundChannel")
    public MessageChannel dataSourceInboundChannel() {
        return new DirectChannel();
    }

    @Bean(name="mqttOutChannel")
    public MessageChannel mqttOutChannel() {
        return new DirectChannel();
    }



    @Bean
    @ServiceActivator(inputChannel="mqttOutChannel")
    public MessageHandler mqttOutbound() {
        MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler("vehiclePublisher", mqttClientFactory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic("vehicleTopic");
        return messageHandler;
    }



    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = new MqttConnectOptions();
        options.setServerURIs(new String[] { "tcp://localhost:1883" });
        options.setMaxInflight(1000);
        factory.setConnectionOptions(options);
        return factory;
    }

    @Bean(name="vehicleDataChannel")
    public MessageChannel vehicleDataChannel() {
        return new DirectChannel();
    }

    @ServiceActivator(inputChannel="torqueInboundChannel")
    @Bean
    public MessageHandler loggingHandler()
    {
        LoggingHandler h = new LoggingHandler("info");
        h.setShouldLogFullMessage(true);
        return h;
    }

}
