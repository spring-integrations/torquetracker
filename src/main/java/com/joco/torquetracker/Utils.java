package com.joco.torquetracker;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

public class Utils {
    public static final String LAT_TAG = "LAT";
    public static final String LON_TAG = "LON";
    public static final String EVENT_TIME_TAG = "EVENT_TIME";

    public static JsonNode loadJsonFromFile(String configurationFile) throws IOException {
        File file;
        if (configurationFile.startsWith("classpath")) {
            file = ResourceUtils.getFile(configurationFile);
        }
        else {
            file = new File(configurationFile);
        }


        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(file);
    }

    public static boolean isDouble(String stringToCheck) {
        try {
           Double d = Double.valueOf(stringToCheck);
           return true;
        }
        catch (NumberFormatException ex) {
            return false;
        }

    }
}
