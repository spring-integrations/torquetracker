package com.joco.torquetracker.torque;

import com.joco.torquetracker.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class TorqueDataSource {

    private final MessageChannel dataSourceChannel;

    @Autowired
    public TorqueDataSource(@Qualifier("dataSourceInboundChannel") MessageChannel dataSourceChannel) {
        this.dataSourceChannel = dataSourceChannel;
    }

    @ServiceActivator(inputChannel = "torqueInboundChannel")
    public Message<?> getRequest(Message<MultiValueMap<String, String>> input) {

        this.dataSourceChannel.send(convert(input));
        return MessageBuilder.withPayload("OK!")
                .setHeader("http_statusCode", 200)
                .build();

    }


    public Message<?> convert(Message<MultiValueMap<String, String>> requestMessage) {
        Map<String, String> requestPayload = requestMessage.getPayload().toSingleValueMap();

        Map<String, Double> convertedPayload = requestPayload.entrySet()
                .stream()
                .filter(e -> Utils.isDouble(e.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> Double.valueOf(entry.getValue())));



        return MessageBuilder.withPayload(convertedPayload).build();
    }

}
