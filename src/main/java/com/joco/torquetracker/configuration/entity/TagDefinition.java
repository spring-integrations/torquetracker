package com.joco.torquetracker.configuration.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class TagDefinition {
    private String name;
    private String description;
    private String unit;

}