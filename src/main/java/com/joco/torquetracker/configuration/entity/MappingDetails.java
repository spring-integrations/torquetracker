package com.joco.torquetracker.configuration.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MappingDetails {
    private String sourceTag;
    private String sourceUnit;
    private String internalTag;

    public MappingDetails(MappingDetails mappingDetails) {
        sourceTag = mappingDetails.getSourceTag();
        sourceUnit = mappingDetails.getSourceUnit();
        internalTag = mappingDetails.getInternalTag();
    }
}
