package com.joco.torquetracker.configuration.boundary;

import com.fasterxml.jackson.databind.JsonNode;
import com.joco.torquetracker.configuration.control.ConfigurationService;
import com.joco.torquetracker.configuration.control.MsConfigurationException;
import com.joco.torquetracker.configuration.entity.MappingDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.joco.torquetracker.Utils.loadJsonFromFile;

@Component
@Slf4j
public class SourceTagMapper {

    private Map<String, MappingDetails> tagMap;
    private String fileName;

    private final ConfigurationService configurationService;

    @Autowired
    public SourceTagMapper(@Value("${tagMappingFile}") String fileName, ConfigurationService configurationService) throws IOException {
        this.fileName = fileName;
        this.configurationService = configurationService;
    }

    public Map<String, MappingDetails> getMapping()  {

        if (tagMap == null) {
            try {
                tagMap = loadMapping();
            } catch (IOException e) {
                throw new MsConfigurationException("Error loading source mapping",e);
            }
        }
        return tagMap;
    }

    private Map<String, MappingDetails> loadMapping() throws IOException {
        JsonNode jsonNode = loadJsonFromFile(fileName);

        return StreamSupport.stream(jsonNode.spliterator(), false)
                .map(this::createMappingDetails)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(MappingDetails::getSourceTag, v -> v));
    }

    private MappingDetails createMappingDetails(JsonNode item)  {
        String internalTag = item.get("internalTag").asText();
        if (configurationService.getConfiguration().stream().noneMatch(td -> td.getName().equals(internalTag))) {
            log.warn("Internal tag {} is not defined in the configuration, so this mapping will be ignored ");
            return null;
        }

        return new MappingDetails(item.get("sourceTag").asText(), item.get("sourceUnit").asText(), item.get("internalTag").asText());
    }

}

