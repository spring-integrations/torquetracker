package com.joco.torquetracker.configuration.control;


public class MsConfigurationException extends RuntimeException {
    public MsConfigurationException() {
    }

    public MsConfigurationException(String s) {
        super(s);
    }

    public MsConfigurationException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public MsConfigurationException(Throwable throwable) {
        super(throwable);
    }

    public MsConfigurationException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
