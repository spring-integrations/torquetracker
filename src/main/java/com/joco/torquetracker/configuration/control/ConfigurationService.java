package com.joco.torquetracker.configuration.control;

import com.fasterxml.jackson.databind.JsonNode;
import com.joco.torquetracker.configuration.entity.TagDefinition;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.joco.torquetracker.Utils.loadJsonFromFile;

@Component
public class ConfigurationService {

    private List<TagDefinition> configuration;
    private String configurationFile;

    public ConfigurationService(@Value("${configurationFile}") String fileName) {
        configurationFile = fileName;
    }

    public List<TagDefinition> getConfiguration() {
        try {
            if (configuration == null) {
                this.configuration = loadConfiguration();
            }

            return this.configuration;
        }
        catch (IOException e) {
            throw new MsConfigurationException("Error loading configuration.", e);
        }
    }

    private List<TagDefinition> loadConfiguration() throws IOException {
        JsonNode jsonNode = loadJsonFromFile(configurationFile);
        return StreamSupport.stream(jsonNode.spliterator(), false).map(item -> new TagDefinition(item.get("name").asText(), item.get("description").asText(), item.get("unit").asText())).collect(Collectors.toList());
    }


}
