package com.joco.torquetracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorquetrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TorquetrackerApplication.class, args);
    }

}
