package com.joco.measurement;

import com.joco.torquetracker.vehicledata.control.VehicleDataProcessor;
import com.joco.torquetracker.vehicledata.control.InvalidLocationException;
import com.joco.torquetracker.vehicledata.entity.VehicleData;
import com.joco.torquetracker.configuration.control.ConfigurationService;
import com.joco.torquetracker.configuration.entity.MappingDetails;
import com.joco.torquetracker.configuration.boundary.SourceTagMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.measure.IncommensurableException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static tec.units.ri.unit.MetricPrefix.CENTI;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MeasurementApplicationTests {

    private ConfigurationService configurationService;
    private SourceTagMapper tagMapper;

    @Before
    public void setUp() throws IOException {

        this.configurationService = new ConfigurationService("classpath:tag_configuration.json");
        this.tagMapper = new SourceTagMapper("classpath:tag_mapping.json",configurationService);


    }

    @Test
    public void configurationLoads() {

        assertThat(configurationService.getConfiguration().size(), is(5));
    }


    @Test
    public void tagMappingLoads() throws IOException {
        Map<String, MappingDetails> mapping = tagMapper.getMapping();
        assertThat(mapping.get("kff1006"), is(not(nullValue())));
    }

    @Test
    public void processValues_withValuesPresent_givesValidResult() throws IOException, IncommensurableException {
        VehicleDataProcessor vehicleDataProcessor = new VehicleDataProcessor(configurationService, tagMapper);
        Map<String, Double> inputValues = new HashMap<>();
        inputValues.put("kff1006", 51.8);
        inputValues.put("kff1005", 5.8);
        inputValues.put("kff1001", 60.0);
        inputValues.put("kff1009", 60.0);
        inputValues.put("kff05", 90.0);


        VehicleData actualVehicleData = vehicleDataProcessor.processValues(inputValues);

        assertThat(actualVehicleData.getLat(), is(51.8));
        assertThat(actualVehicleData.getLon(), is(5.8));
        assertThat(actualVehicleData.getTagValues().size(), is (3));
        assertThat(actualVehicleData.getTagValues().get("GPS_SPEED"), is(60/3.6));
        assertThat(actualVehicleData.getTagValues().get("OBD_SPEED"), is(60/3.6));
        assertThat(actualVehicleData.getTagValues().get("COOLANT_TEMP"), is(90.0));
    }

    @Test
    public void processValues_withNullValuesForLatitudeAndLongitudePresent_throwsInvalidLocationException() {
        VehicleDataProcessor vehicleDataProcessor = new VehicleDataProcessor(configurationService, tagMapper);
        Map<String, Double> inputValues = new HashMap<>();
        inputValues.put("kff1006", null);
        inputValues.put("kff1005", null);
        inputValues.put("kff1001", 60.0);
        inputValues.put("kff1009", 60.0);
        inputValues.put("kff05", 90.0);

        assertThrows(InvalidLocationException.class, () -> vehicleDataProcessor.processValues(inputValues));
    }

    @Test
    public void processValues_withValuesForLatitudeAndLongitudeNotPresent_throwsInvalidLocationException() {
        VehicleDataProcessor vehicleDataProcessor = new VehicleDataProcessor(configurationService, tagMapper);
        Map<String, Double> inputValues = new HashMap<>();
        inputValues.put("kff1001", 60.0);
        inputValues.put("kff1009", 60.0);
        inputValues.put("kff05", 90.0);

        assertThrows(InvalidLocationException.class, () -> vehicleDataProcessor.processValues(inputValues));
    }


    @Test
    public void processValues_withNonMappedTags_givesResultWithoutThoseTags() throws IOException, IncommensurableException {
        VehicleDataProcessor vehicleDataProcessor = new VehicleDataProcessor(configurationService, tagMapper);
        Map<String, Double> inputValues = new HashMap<>();
        inputValues.put("kff1006", 51.8);
        inputValues.put("kff1005", 5.8);
        inputValues.put("kff1001", 60.0);
        inputValues.put("kff1009", 60.0);
        inputValues.put("kff05", 90.0);
        inputValues.put("kff1010", 8.0);


        VehicleData actualVehicleData = vehicleDataProcessor.processValues(inputValues);

        assertThat(actualVehicleData.getLat(), is(51.8));
        assertThat(actualVehicleData.getLon(), is(5.8));
        assertThat(actualVehicleData.getTagValues().size(), is(3));
        assertThat(actualVehicleData.getTagValues().get("GPS_SPEED"), is(60 / 3.6));
        assertThat(actualVehicleData.getTagValues().get("OBD_SPEED"), is(60 / 3.6));
        assertThat(actualVehicleData.getTagValues().get("COOLANT_TEMP"), is(90.0));
    }

    @Test
    public void processValues_withUndefinedSourceUnit_givesResultWithoutThoseTags() throws IOException, IncommensurableException {

        SourceTagMapper tagMapperWithInvalidUnit = new SourceTagMapper("classpath:tag_mapping_undefined_unit.json", configurationService);
        VehicleDataProcessor vehicleDataProcessor = new VehicleDataProcessor(configurationService, tagMapperWithInvalidUnit);
        Map<String, Double> inputValues = new HashMap<>();
        inputValues.put("kff1006", 51.8);
        inputValues.put("kff1005", 5.8);
        inputValues.put("kff1001", 60.0);
        inputValues.put("kff1009", 60.0);
        inputValues.put("kff05", 90.0);


        VehicleData actualVehicleData = vehicleDataProcessor.processValues(inputValues);

        assertThat(actualVehicleData.getLat(), is(51.8));
        assertThat(actualVehicleData.getLon(), is(5.8));
        assertThat(actualVehicleData.getTagValues().size(), is(2));
    }

    @Test
    public void processValues_withInvalidQuantity_givesResultWithoutThoseTags() throws IOException, IncommensurableException {

        SourceTagMapper tagMapperWithInvalidUnit = new SourceTagMapper("classpath:tag_mapping_invalid_quantity.json", configurationService);
        VehicleDataProcessor vehicleDataProcessor = new VehicleDataProcessor(configurationService, tagMapperWithInvalidUnit);
        Map<String, Double> inputValues = new HashMap<>();
        inputValues.put("kff1006", 51.8);
        inputValues.put("kff1005", 5.8);
        inputValues.put("kff1001", 60.0);
        inputValues.put("kff1009", 60.0);
        inputValues.put("kff05", 90.0);


        VehicleData actualVehicleData = vehicleDataProcessor.processValues(inputValues);

        assertThat(actualVehicleData.getLat(), is(51.8));
        assertThat(actualVehicleData.getLon(), is(5.8));
        assertThat(actualVehicleData.getTagValues().size(), is(2));
    }


}
